cmake_minimum_required(VERSION 3.1.0)

project(ClipExt2)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5Quick CONFIG REQUIRED)
find_package(Qt5Sql CONFIG REQUIRED)

qt5_add_resources(Resources src/qml.qrc)

include_directories(./src)
file(GLOB_RECURSE CppSources src/*.cpp)

add_executable(clipext2 WIN32 ${CppSources} ${Resources} src/appicon.rc)

target_link_libraries(clipext2 Qt5::Quick ${CONAN_LIBS})
