#include "GlobalShortcut.h"

#include <QDebug>

#ifdef Q_OS_WIN
#include "windows.h"
#endif

GlobalShortcut::GlobalShortcut(QCoreApplication &app)
{
#ifdef Q_OS_WIN
    if (RegisterHotKey(nullptr, 1, MOD_ALT | 0x4000, 86))  //86 is 'v'
    {
        qDebug() << "Hotkey 'Alt+V' registered, using MOD_NOREPEAT flag";
    }
#endif

    app.installNativeEventFilter(this);
}

bool GlobalShortcut::nativeEventFilter(const QByteArray &, void *message, long *)
{
#ifdef Q_OS_WIN
    MSG* msg = static_cast<MSG*>(message);

    if(msg->message == WM_HOTKEY)
    {
        qDebug() << "WM_HOTKEY !";
        emit ShortcutPressed();
    }
#endif

    return false;
}
