import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import ClipExt 1.0

Control {
    id: root

    property ClipboardModel clipboardModel: ClipboardModel {
        clipboardItemsStore: ClipboardItemsStore
    }

    signal itemSelected(var item)
    signal clipboardItemDoubleClicked(var item)

    onFocusChanged: {
        if (focus) {
            filterField.forceActiveFocus()
        }
    }

    Keys.onPressed: {
        if (event.key === Qt.Key_Enter || event.key === Qt.Key_Return) {
            event.accepted = true

            clipboardItemDoubleClicked(itemsList.currentItem.item)
        }
    }

    Keys.forwardTo: [itemsList, filterField]

    Rectangle {
        id: filterFieldShadowWrapper

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: childrenRect.height

        TextField {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right

            id: filterField
            placeholderText: qsTr("Search...")
            selectByMouse: true
            padding: 10

            font.pointSize: 12
            background: Rectangle {
                color: "transparent"
                border.color: "lightgray"
            }

            Timer {
                id: searchInputTimer
                interval: 500
                repeat: false
                onTriggered: {
                    clipboardModel.setSearchQuery(parent.text)
                }
            }

            onTextEdited: {
                searchInputTimer.restart()
            }
        }
    }

    DropShadow {
        anchors.fill: filterFieldShadowWrapper
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: filterFieldShadowWrapper
    }

    ListView {
        id: itemsList

        anchors.top: filterFieldShadowWrapper.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        clip: true

        model: clipboardModel

        delegate: ColumnLayout {
            property variant item: model.item;

            width: parent.width
            spacing: 0

            Rectangle {
                Layout.fillWidth: true
                height: 1
                color: "transparent"
            }

            Item {
                Layout.fillWidth: true
                height: childrenRect.height

                Text {
                    id: text

                    padding: {
                        left: 10
                        top: 9
                        bottom: 9
                    }

                    text: model.text.slice(0, 100)
                    wrapMode: Text.NoWrap
                    elide: Text.ElideRight
                    maximumLineCount: 1
                    textFormat: Text.PlainText
                }

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        itemsList.currentIndex = index
                    }

                    onDoubleClicked: {
                        root.clipboardItemDoubleClicked(item)
                    }
                }
            }


            Rectangle {
                Layout.fillWidth: true
                height: 1
                color: "lightgray"
            }
        }

        onCurrentIndexChanged: {
            itemSelected(itemsList.currentItem.item)
        }

        highlight: Rectangle { color: "lightgray" }
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 100


        ScrollBar.vertical: ScrollBar {}
    }
}
