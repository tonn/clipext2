#ifndef CLIPBOARDITEM_H
#define CLIPBOARDITEM_H

#include <QDate>
#include <QString>
#include <QObject>

struct ClipboardItem
{
    Q_GADGET
    Q_PROPERTY(int Id MEMBER Id)
    Q_PROPERTY(QString Text MEMBER Text)
    Q_PROPERTY(QDateTime UseDate MEMBER UseDate)

public:
    int Id;
    QString Text;
    QDateTime UseDate;

    ClipboardItem() = default;
    ClipboardItem(const ClipboardItem & item);

    bool operator== (const ClipboardItem & item) const;
};

Q_DECLARE_METATYPE(ClipboardItem)

#endif // CLIPBOARDITEM_H
