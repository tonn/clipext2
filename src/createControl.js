.pragma library
.import QtQuick 2.11 as QtQuick

var create = (function () {
    var controlTypesCache = []

    return function (url, parent) {
        if (!(url in controlTypesCache)) {
            controlTypesCache[url] = Qt.createComponent(url, QtQuick.Component.PreferSynchronous, parent)

            var error = controlTypesCache[url].errorString()

            if (error) {
                console.error(error)
            }
        }

        var controlType = controlTypesCache[url]
        var control = controlType.createObject(parent)

        console.debug(url + ' created')

        if (control.closing) {
            control.closing.connect(function () {
                control.destroy()
                console.debug(url + ' destroyed')
            })
        }

        return control
    }
})()
