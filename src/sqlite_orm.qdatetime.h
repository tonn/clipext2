#include <QDate>
#include <sqlite_orm/sqlite_orm.h>

namespace sqlite_orm {
    template<>
    struct type_printer<QDateTime> : public integer_printer {};

    template<>
    struct statement_binder<QDateTime> {
        int bind(sqlite3_stmt *stmt, int index, const QDateTime &value) {
            return statement_binder<long long>().bind(stmt, index, value.toTime_t());
        }
    };

    template<>
    struct field_printer<QDateTime> {
        long long operator()(const QDateTime & value) const {
            return value.toTime_t();
        }
    };

    template<>
    struct row_extractor<QDateTime> {
        QDateTime extract(long long row_value) {
            return QDateTime::fromTime_t(row_value);
        }

        QDateTime extract(sqlite3_stmt *stmt, int columnIndex) {
            auto row_value = sqlite3_column_int64(stmt, columnIndex);
            return this->extract(row_value);
        }
    };
}
