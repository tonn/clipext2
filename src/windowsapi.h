#ifndef WINDOWSAPI_H
#define WINDOWSAPI_H

#include <Windows.h>

struct Point {
    int x;
    int y;
};

void Paste();

Point GetCaretPosition();
Point GetForegroundWindowCenter();

#endif // WINDOWSAPI_H
