#include <QString>
#include <sqlite_orm/sqlite_orm.h>

namespace sqlite_orm {
    template<>
    struct type_printer<QString> : public text_printer {};

    template<>
    struct statement_binder<QString> {
        int bind(sqlite3_stmt *stmt, int index, const QString &value) {
            return statement_binder<std::string>().bind(stmt, index, value.toStdString());
        }
    };

    template<>
    struct field_printer<QString> {
        std::string operator()(const QString &t) const {
            return t.toStdString();
        }
    };

    template<>
    struct row_extractor<QString> {
        QString extract(const char *row_value) {
            return QString(row_value);
        }

        QString extract(sqlite3_stmt *stmt, int columnIndex) {
            auto str = sqlite3_column_text(stmt, columnIndex);
            return this->extract((const char*)str);
        }
    };
}
