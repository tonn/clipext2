#ifndef ClipboardItemsStore_h
#define ClipboardItemsStore_h

#include <QObject>
#include <QVector>
#include <sqlite_orm/sqlite_orm.h>
#include "sqlite_orm.qstring.h"
#include "sqlite_orm.qdatetime.h"

#include "ClipboardItem.h"

using namespace sqlite_orm;

inline auto initStorage() {
    using namespace sqlite_orm;
    return make_storage("db.sqlite",
        make_table("history",
            make_column("id", &ClipboardItem::Id, autoincrement(), primary_key()),
            make_column("text", &ClipboardItem::Text),
            make_column("useDate", &ClipboardItem::UseDate)));
}

using SqliteOrmStorage = decltype(initStorage());

class ClipboardItemsStore : public QObject
{
    Q_OBJECT
public:
    explicit ClipboardItemsStore(QObject *parent = nullptr);

    QVector<ClipboardItem> items() const;
    void addTextItem(QString & text);

signals:
    void preItemsChanged();
    void postItemsChanged();

    void preItemPrepended();
    void postItemPrepended();

    void preItemRemoved(int index);
    void postItemRemoved();

private:
    QVector<ClipboardItem> _items;

    void insert(ClipboardItem & item);
    void update(ClipboardItem & item);
    void updateAndMoveToTop(ClipboardItem &item);
    void remove(const ClipboardItem & item);

    SqliteOrmStorage _storage;
};

#endif
