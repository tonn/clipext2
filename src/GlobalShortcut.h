#ifndef GLOBALSHORTCUT_H
#define GLOBALSHORTCUT_H

#include <QAbstractNativeEventFilter>
#include <QObject>
#include <QCoreApplication>

class GlobalShortcut: public QObject, QAbstractNativeEventFilter
{
    Q_OBJECT

public:
    GlobalShortcut(QCoreApplication& app);

    bool nativeEventFilter(const QByteArray& eventType, void* message, long*);

signals:
    void ShortcutPressed();
};

#endif // GLOBALSHORTCUT_H
