#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include "ClipboardItemsStore.h"

#include <QClipboard>
#include <QObject>
#include <QGuiApplication>

class Clipboard : public QObject
{
    Q_OBJECT

    QClipboard * _clipboard;
    ClipboardItemsStore * _store;

public:
    Clipboard(QGuiApplication & app, ClipboardItemsStore & store);

signals:
    void Changed(QString text);

private slots:
    void clipboardChanged();

public slots:
    void paste(ClipboardItem item);

    int getForegroundWindowX();
    int getForegroundWindowY();
};

#endif // CLIPBOARD_H
