import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0

Window {
    id: mainWindow
    flags: Qt.SplashScreen | Qt.WindowStaysOnTopHint

    visible: true
    width: 480 * 2
    height: 480

    x: (Screen.width / 2 - width / 2)
    y: (Screen.height / 2 - height / 2)

    color: 'transparent'

    Component.onCompleted: {
//        mainWindow.Screen = Qt.application.screens[0]
        itemsList.forceActiveFocus()
    }

    Rectangle {
        id: rect
        anchors.fill: parent
        border.color: 'lightgray'
        border.width: 1

        Keys.onEscapePressed: {
            event.accepted = true
            close()
        }

        RowLayout {
            anchors.fill: parent

            ClipboardItemsList {
                id: itemsList
                Layout.fillHeight: true
                Layout.fillWidth: true

                onClipboardItemDoubleClicked: {
                    mainWindow.close()
                    Clipboard.paste(item)
                }

                onItemSelected: {
                    preview.item = item
                }
            }

            Rectangle {
                width: 1
                Layout.fillHeight: true
                color: 'lightgray'
            }

            Preview {
                id: preview
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }
}
