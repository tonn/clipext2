#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMenu>
#include <QtWidgets/QSystemTrayIcon>
#include <QtWidgets/qaction.h>

#include "Clipboard.h"
#include "ClipboardItemsStore.h"
#include "ClipboardModel.h"
#include "GlobalShortcut.h"

#define QmlModuleName "ClipExt"
#define QmlClipboardModelTypeName "ClipboardModel"
#define QmlClipboardItemsStoreTypeName "ClipboardItemsStoreType"
#define QmlClipboardItemsStoreName "ClipboardItemsStore"
#define QmlGlobalShortcutTypeName "GlobalShortcutType"
#define QmlGlobalShortcutName "GlobalShortcut"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);

    qmlRegisterType<ClipboardModel>(QmlModuleName, 1, 0, QmlClipboardModelTypeName);
    qmlRegisterUncreatableType<ClipboardItemsStore>(QmlModuleName, 1, 0, QmlClipboardItemsStoreTypeName, QStringLiteral("should not be created in QML"));
    qmlRegisterUncreatableType<GlobalShortcut>(QmlModuleName, 1, 0, QmlGlobalShortcutTypeName, QStringLiteral("should not be created in QML"));
    qmlRegisterUncreatableType<Clipboard>(QmlModuleName, 1, 0, "ClipboardType", "should not be created in QML");

    ClipboardItemsStore store;
    GlobalShortcut globalShortcut(app);
    Clipboard clipboard(app, store);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QmlClipboardItemsStoreName, &store);
    engine.rootContext()->setContextProperty(QmlGlobalShortcutName, &globalShortcut);
    engine.rootContext()->setContextProperty("Clipboard", &clipboard);
    engine.load("qrc:/TrayIcon.qml");
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
