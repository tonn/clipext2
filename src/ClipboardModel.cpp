#include "ClipboardModel.h"

#include "ClipboardItemsStore.h"
#include <QDebug>

ClipboardModel::ClipboardModel(QObject *parent)
    : QAbstractListModel(parent)
    , _clipboardItemsStore(nullptr)
{
    _pageSize = 100;
    _currentPage = 0;
    _searchQuery = "";
}

int ClipboardModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    auto result = currentCount();

    return result;
}

QVariant ClipboardModel::data(const QModelIndex &index, int role) const
{
    QVariant result;

    if (index.isValid()) {
        const ClipboardItem item = _filteredItems.at(index.row());
        switch (role) {
        case TextRole:
            result.setValue(item.Text);
            break;
        case ItemRole:
            result.setValue(item);
            break;
        }
    }

    return result;
}

//bool ClipboardModel::setData(const QModelIndex &index, const QVariant &value, int role)
//{
//    if (!_clipboardItemsStore)
//        return false;

//    ClipboardItem item = _clipboardItemsStore->items().at(index.row());
//    switch (role) {
//    case TextRole:
//        item.Text= value.toString();
//        break;
//    }

//    if (_clipboardItemsStore->setItemAt(index.row(), item)) {
//        emit dataChanged(index, index, QVector<int>() << role);
//        return true;
//    }
//    return false;
//}

Qt::ItemFlags ClipboardModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ClipboardModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[TextRole] = "text";
    names[ItemRole] = "item";
    return names;
}

ClipboardItemsStore *ClipboardModel::list() const
{
    return _clipboardItemsStore;
}

void ClipboardModel::setList(ClipboardItemsStore *list)
{
    beginResetModel();

    if (_clipboardItemsStore)
        _clipboardItemsStore->disconnect(this);

    _clipboardItemsStore = list;

    if (_clipboardItemsStore) {
        connect(_clipboardItemsStore, &ClipboardItemsStore::postItemsChanged, this, [=]() {
            setSearchQuery(_searchQuery);
        });
    }

    setSearchQuery(_searchQuery);

    endResetModel();
}

bool ClipboardModel::canFetchMore(const QModelIndex &) const
{
    auto result = _currentPage * _pageSize < _filteredItems.size();

    qDebug() << "ClipboardModel::canFetchMore " << result;

    return result;
}

void ClipboardModel::fetchMore(const QModelIndex &)
{
    int currentCount = _pageSize * _currentPage;
    int newCount = _pageSize * (_currentPage + 1);
    int count = _filteredItems.size();

    if (count < newCount) {
        newCount = count;
    }

    beginInsertRows(QModelIndex(), currentCount, newCount - 1);

    ++_currentPage;

    endInsertRows();

    qDebug() << "ClipboardModel::fetchMore " << _currentPage;
}

int ClipboardModel::currentCount() const
{
    int countByPages = _pageSize * _currentPage;
    int count = _filteredItems.size();

    auto result = count < countByPages ? count : countByPages;

    return result;
}

void ClipboardModel::setSearchQuery(QString query)
{
    qDebug() << "ClipboardModel::setSearchQuery " << query;

    beginResetModel();

    _searchQuery = query;

    _filteredItems.clear();

    if (query.length() > 0) {
        foreach (auto item, _clipboardItemsStore->items()) {
            if (item.Text.contains(query, Qt::CaseInsensitive)) {
                _filteredItems.push_back(item);
            }
        }
    } else {
        _filteredItems = _clipboardItemsStore->items();
    }

    _currentPage = 0;

    endResetModel();
}
