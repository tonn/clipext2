#include "windowsapi.h"
#include <QDebug>

void Paste()
{
    // Create a generic keyboard event structure
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;

    // Press the "Ctrl" key
    ip.ki.wVk = VK_CONTROL;
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));

    // Press the "V" key
    ip.ki.wVk = 'V';
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));

    // Release the "V" key
    ip.ki.wVk = 'V';
    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ip, sizeof(INPUT));

    // Release the "Ctrl" key
    ip.ki.wVk = VK_CONTROL;
    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ip, sizeof(INPUT));
}

Point GetCaretPosition()
{
    auto hwnd = GetForegroundWindow();

    RECT windowRect;

    GetWindowRect(hwnd, &windowRect);

    auto targetThreadId = GetWindowThreadProcessId(hwnd, nullptr);

    auto currentThreadId = GetCurrentThreadId();

    AttachThreadInput(targetThreadId, currentThreadId, true);

    POINT caretPosition;

    auto getCaretPosResult = GetCaretPos(&caretPosition);

    ClientToScreen(hwnd, &caretPosition);

    AttachThreadInput(targetThreadId, currentThreadId, false);

    Point result;

    //if (windowRect.top == caretPosition.y && windowRect.left == caretPosition.x) {
        result.x = windowRect.left + (windowRect.right - windowRect.left) / 2;
        result.y = windowRect.top + (windowRect.bottom - windowRect.top) / 2;
    //} else {
//        result.x = caretPosition.x;
//        result.y = caretPosition.y;
//    }

    return result;
}

Point GetForegroundWindowCenter()
{
    auto hwnd = GetForegroundWindow();

    RECT windowRect;

    GetWindowRect(hwnd, &windowRect);

    Point result;

    result.x = windowRect.left + (windowRect.right - windowRect.left) / 2;
    result.y = windowRect.top + (windowRect.bottom - windowRect.top) / 2;

    return result;
}
