#ifndef ClipboardModel_h
#define ClipboardModel_h

#include "ClipboardItem.h"

#include <QAbstractListModel>

class ClipboardItemsStore;

class ClipboardModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(ClipboardItemsStore * clipboardItemsStore READ list WRITE setList)

public:
    explicit ClipboardModel(QObject *parent = nullptr);

    enum {
        TextRole = Qt::UserRole,
        ItemRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
//    bool setData(const QModelIndex &index, const QVariant &value,
//                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    ClipboardItemsStore *list() const;
    void setList(ClipboardItemsStore *list);

    virtual bool canFetchMore(const QModelIndex &parent) const override;
    virtual void fetchMore(const QModelIndex &parent) override;

public slots:
    void setSearchQuery(QString query);

private:
    ClipboardItemsStore *_clipboardItemsStore;

    int _pageSize;
    int _currentPage;

    QString _searchQuery;

    QVector<ClipboardItem> _filteredItems;

    int currentCount() const;
};

#endif
