#include "Clipboard.h"

#include <QDebug>
#include <QGuiApplication>
#include <QMimeData>
#include <QTimer>
#include "windowsapi.h"

const auto& WindowsPaste = Paste;

Clipboard::Clipboard(QGuiApplication &app, ClipboardItemsStore &store)
{
    _clipboard = app.clipboard();

    connect(_clipboard, SIGNAL(dataChanged()), this, SLOT(clipboardChanged()));

    _store = &store;
}

void Clipboard::clipboardChanged()
{
    QTimer::singleShot(100, this, [=]() {
        auto text = _clipboard->text();
        auto mime = _clipboard->mimeData();

        qDebug() << mime->formats() << mime->text() << " | " << text;

        if (text != nullptr || !text.isEmpty()) {
            _store->addTextItem(text);

            emit Changed(text);
        }
    });
}

void Clipboard::paste(ClipboardItem item)
{
    _clipboard->setText(item.Text);

    WindowsPaste();
}

int Clipboard::getForegroundWindowX()
{
    return GetForegroundWindowCenter().x;
}

int Clipboard::getForegroundWindowY()
{
    return GetForegroundWindowCenter().y;
}
