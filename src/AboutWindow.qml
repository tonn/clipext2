import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import QtQuick.Layouts 1.3

Window {
    id: aboutWindow
    flags: Qt.Dialog
    title: 'About ClipExt2'
    minimumWidth: 320
    minimumHeight: 240
    maximumWidth: minimumWidth
    maximumHeight: minimumHeight
    visible: true

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            text: qsTr("<h1>ClipExt2</h1><br><br>Extended clipboard. Enjoy :)<br><br>Anton Novikov (c) 2018")
        }

        Button {
            Layout.alignment: Qt.AlignCenter
            text: 'Close'
            onClicked: aboutWindow.close()
        }
    }
}
