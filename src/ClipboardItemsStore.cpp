#include "ClipboardItemsStore.h"

#include <QVector>
#include <QDebug>

ClipboardItemsStore::ClipboardItemsStore(QObject *parent) :
    QObject(parent),
    _storage(initStorage())
{
    _storage.sync_schema();

    auto items = _storage.get_all<ClipboardItem, QVector<ClipboardItem>>(
                multi_order_by(order_by(&ClipboardItem::UseDate).desc(), order_by(&ClipboardItem::Id)));

    this->_items.append(items);
}

QVector<ClipboardItem> ClipboardItemsStore::items() const
{
    return _items;
}

void ClipboardItemsStore::addTextItem(QString & text)
{
    ClipboardItem item;
    item.UseDate = QDateTime::currentDateTime();
    item.Text = text;

    // Ищем такие же записи в базе
    auto sameRecords = _storage.get_all<ClipboardItem, QVector<ClipboardItem>>(
                where(c(&ClipboardItem::Text) == text),
                order_by(&ClipboardItem::UseDate));

    if (sameRecords.length() == 0)
    {
        insert(item);
    }
    else
    {
        for (int i = 0; i < sameRecords.length(); ++i)
        {
            auto item = sameRecords[i];

            if (i != 0)
            {
                remove(item);
            }
            else
            {
                item.UseDate = QDateTime::currentDateTime();

                updateAndMoveToTop(item);
            }
        }
    }
}

void ClipboardItemsStore::insert(ClipboardItem & item)
{
    emit preItemsChanged();

    item.Id = _storage.insert(item);

    _items.prepend(item);

    emit postItemsChanged();
}

void ClipboardItemsStore::update(ClipboardItem & item)
{
    emit preItemsChanged();

    _storage.update(item);

    emit postItemsChanged();
}

void ClipboardItemsStore::updateAndMoveToTop(ClipboardItem & item)
{
    emit preItemsChanged();

    _storage.update(item);

    auto itemIndexToRemove = _items.indexOf(item);

    _items.remove(itemIndexToRemove);
    _items.prepend(item);

    emit postItemsChanged();
}

void ClipboardItemsStore::remove(const ClipboardItem & item)
{
    _storage.remove<ClipboardItem>(item.Id);

    auto itemIndexToRemove = _items.indexOf(item);

    preItemRemoved(itemIndexToRemove);

    _items.remove(itemIndexToRemove);

    postItemRemoved();
}
