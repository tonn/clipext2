import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import ClipExt 1.0

Control {
    id: root
    property var item

    ScrollView {
        anchors.fill: parent

        ScrollBar.horizontal.policy: ScrollBar.AsNeeded
        ScrollBar.vertical.policy: ScrollBar.AsNeeded

        TextArea {
            text: root.item.Text
            textFormat: TextEdit.PlainText
            readOnly: true
            selectByKeyboard: true
            selectByMouse: true
            font.family: 'Consolas'
        }
    }
}
