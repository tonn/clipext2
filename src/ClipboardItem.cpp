#include "ClipboardItem.h"

ClipboardItem::ClipboardItem(const ClipboardItem &item)
{
    Id = item.Id;
    Text = item.Text;
    UseDate = item.UseDate;
}

bool ClipboardItem::operator==(const ClipboardItem &item) const
{
    return item.Id == Id;
}
