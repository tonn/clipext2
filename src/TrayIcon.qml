import QtQuick 2.11
import Qt.labs.platform 1.0
import "createControl.js" as CreateControl;

SystemTrayIcon {
    id: trayIcon
    visible: true
    iconSource: "qrc:/clipboard.png"

    menu: Menu {
        Loader { id: menuLoader }

        MenuItem {
            text: qsTr('About')
            onTriggered: {
                CreateControl.create('qrc:/AboutWindow.qml', trayIcon)
            }
        }

        MenuItem {
            text: qsTr("Quit")
            onTriggered: Qt.quit()
        }
    }

    function showMainWindow() {
        var mainWindow = CreateControl.create('qrc:/MainWindow.qml', trayIcon)

        mainWindow.requestActivate()

        mainWindow.onActiveChanged.connect(function () {
            console.debug('mainWindow active changed: ' + mainWindow.active)
            if (!mainWindow.active) {
                mainWindow.close()
            }
        })

        return mainWindow;
    }

    Component.onCompleted: {
        GlobalShortcut.ShortcutPressed.connect(function () {
            var window = showMainWindow()

            var x = Clipboard.getForegroundWindowX()
            var y = Clipboard.getForegroundWindowY()

            window.setX(x - window.width / 2)
            window.setY(y - window.height / 2)
        })
    }

    onActivated: {
        if (reason === SystemTrayIcon.Trigger) {            
            showMainWindow();
        }
    }
}
