QtCreator:

1. Install conan
2. Create folder `<root>/build`
3. Run `conan install ..` from `<root>/build`
4. Open cmake-project from QtCreator
5. Set build diretory to `<root>/build`
6. Build - Run CMake
